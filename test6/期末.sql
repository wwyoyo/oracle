--------------------------------------------------------
--  文件已创建 - 星期一-五月-22-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ORDER_DETAILS
--------------------------------------------------------

Create Tablespace wq1
datafile
'/home/oracle/app/oracle/oradata/orcl/wq1_1.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/wq1_2.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

Create Tablespace wq2
datafile
'/home/oracle/app/oracle/oradata/orcl/wq2_1.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/wq2_2.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;



  CREATE TABLE "SYSTEM"."ORDER_DETAILS" 
   (	"ORDER_DETAIL_ID" VARCHAR2(20 BYTE), 
	"ORDER_ID" NUMBER(10,0), 
	"PRODUCT_ID" NUMBER(10,0), 
	"QUANTITY" NUMBER(10,0), 
	"UNIT_PRICE" NUMBER(10,2)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "WQ1" ;
  GRANT DELETE ON "SYSTEM"."ORDER_DETAILS" TO "WQ";
  GRANT INSERT ON "SYSTEM"."ORDER_DETAILS" TO "WQ";
  GRANT SELECT ON "SYSTEM"."ORDER_DETAILS" TO "WQ";
  GRANT UPDATE ON "SYSTEM"."ORDER_DETAILS" TO "WQ";
--------------------------------------------------------
--  DDL for Index ORDER_DETAILS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."ORDER_DETAILS_PK" ON "SYSTEM"."ORDER_DETAILS" ("ORDER_DETAIL_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table ORDER_DETAILS
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."ORDER_DETAILS" MODIFY ("ORDER_ID" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."ORDER_DETAILS" MODIFY ("PRODUCT_ID" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."ORDER_DETAILS" MODIFY ("QUANTITY" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."ORDER_DETAILS" MODIFY ("UNIT_PRICE" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."ORDER_DETAILS" ADD CONSTRAINT "ORDER_DETAILS_PK" PRIMARY KEY ("ORDER_DETAIL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ORDER_DETAILS
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."ORDER_DETAILS" ADD FOREIGN KEY ("ORDER_ID")
	  REFERENCES "SYSTEM"."ORDERS" ("ORDER_ID") ENABLE;
  ALTER TABLE "SYSTEM"."ORDER_DETAILS" ADD FOREIGN KEY ("PRODUCT_ID")
	  REFERENCES "SYSTEM"."PRODUCTS" ("PRODUCT_ID") ENABLE;

--  DDL for Table CUSTOMERS
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."CUSTOMERS" 
   (	"CUSTOMER_ID" NUMBER(10,0), 
	"CUSTOMER_NAME" VARCHAR2(50 BYTE), 
	"EMAIL" VARCHAR2(100 BYTE), 
	"PHONE_NUMBER" VARCHAR2(15 BYTE), 
	"ADDRESS" VARCHAR2(255 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "WQ1" ;
  GRANT DELETE ON "SYSTEM"."CUSTOMERS" TO "WQ";
  GRANT INSERT ON "SYSTEM"."CUSTOMERS" TO "WQ";
  GRANT SELECT ON "SYSTEM"."CUSTOMERS" TO "WQ";
  GRANT UPDATE ON "SYSTEM"."CUSTOMERS" TO "WQ";
--------------------------------------------------------
--  DDL for Index SYS_C007711
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."SYS_C007711" ON "SYSTEM"."CUSTOMERS" ("CUSTOMER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
  ALTER INDEX "SYSTEM"."SYS_C007711"  UNUSABLE;
--------------------------------------------------------
--  DDL for Index SYS_C007712
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."SYS_C007712" ON "SYSTEM"."CUSTOMERS" ("EMAIL") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
  ALTER INDEX "SYSTEM"."SYS_C007712"  UNUSABLE;
--------------------------------------------------------
--  Constraints for Table CUSTOMERS
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."CUSTOMERS" MODIFY ("CUSTOMER_NAME" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."CUSTOMERS" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."CUSTOMERS" ADD PRIMARY KEY ("CUSTOMER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "SYSTEM"."CUSTOMERS" ADD UNIQUE ("EMAIL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;



--  DDL for Table ORDERS
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."ORDERS" 
   (	"ORDER_ID" NUMBER(10,0), 
	"CUSTOMER_ID" NUMBER(10,0), 
	"ORDER_DATE" DATE DEFAULT SYSDATE, 
	"TOTAL_AMOUNT" NUMBER(10,2)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "WQ1" ;
  GRANT DELETE ON "SYSTEM"."ORDERS" TO "WQ";
  GRANT INSERT ON "SYSTEM"."ORDERS" TO "WQ";
  GRANT SELECT ON "SYSTEM"."ORDERS" TO "WQ";
  GRANT UPDATE ON "SYSTEM"."ORDERS" TO "WQ";
--------------------------------------------------------
--  DDL for Index SYS_C007716
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."SYS_C007716" ON "SYSTEM"."ORDERS" ("ORDER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table ORDERS
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."ORDERS" MODIFY ("CUSTOMER_ID" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."ORDERS" MODIFY ("ORDER_DATE" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."ORDERS" MODIFY ("TOTAL_AMOUNT" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."ORDERS" ADD PRIMARY KEY ("ORDER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ORDERS
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."ORDERS" ADD FOREIGN KEY ("CUSTOMER_ID")
	  REFERENCES "SYSTEM"."CUSTOMERS" ("CUSTOMER_ID") ENABLE;

--  DDL for Table PRODUCTS
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."PRODUCTS" 
   (	"PRODUCT_ID" NUMBER(10,0), 
	"PRODUCT_NAME" VARCHAR2(100 BYTE), 
	"CATEGORY" VARCHAR2(50 BYTE), 
	"PRICE" NUMBER(10,2), 
	"STOCK_QUANTITY" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "WQ1" ;
  GRANT INSERT ON "SYSTEM"."PRODUCTS" TO "WQ";
  GRANT SELECT ON "SYSTEM"."PRODUCTS" TO "WQ";
  GRANT DELETE ON "SYSTEM"."PRODUCTS" TO "WQ";
  GRANT UPDATE ON "SYSTEM"."PRODUCTS" TO "WQ";
--------------------------------------------------------
--  DDL for Index SYS_C007708
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYSTEM"."SYS_C007708" ON "SYSTEM"."PRODUCTS" ("PRODUCT_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table PRODUCTS
--------------------------------------------------------

  ALTER TABLE "SYSTEM"."PRODUCTS" MODIFY ("PRODUCT_NAME" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."PRODUCTS" MODIFY ("PRICE" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."PRODUCTS" MODIFY ("STOCK_QUANTITY" NOT NULL ENABLE);
  ALTER TABLE "SYSTEM"."PRODUCTS" ADD PRIMARY KEY ("PRODUCT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;


--  DDL for inster data 商品表 顾客表 订单表  订单表详情
--------------------------------------------------------

BEGIN
  FOR i IN 1..40000 LOOP
    INSERT INTO products (product_id, product_name, price, stock_quantity)
    VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..40000 LOOP
    INSERT INTO customers (customer_id, customer_name, email, address)
    VALUES (i, 'Customer ' || i,  i || 'qq.com', 'Address ' || i);
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..80000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 40000), 0),  TO_DATE('2000-01-01', 'YYYY-MM-DD') +
  DBMS_RANDOM.VALUE(0, (SYSDATE - TO_DATE('2000-01-01', 'YYYY-MM-DD'))), ROUND(DBMS_RANDOM.VALUE(10, 1000), 0));
  END LOOP;
  COMMIT;
END;

DECLARE
  v_order_id NUMBER;
  v_product_id NUMBER;
  v_count NUMBER; -- 添加此行声明v_count变量
BEGIN
  FOR i IN 1..40000 LOOP
    LOOP
      v_order_id := ROUND(DBMS_RANDOM.VALUE(1, 40000), 0);
      v_product_id := ROUND(DBMS_RANDOM.VALUE(1, 40000), 0);

      -- 检查组合是否已存在
      SELECT COUNT(*)
      INTO   v_count
      FROM   order_details
      WHERE  order_id = v_order_id
      AND    product_id = v_product_id;

      -- 如果组合不存在，则插入数据并跳出循环
      IF v_count = 0 THEN
        EXIT;
      END IF;
    END LOOP;

    INSERT INTO order_details (order_id, product_id, quantity, unit_price)
    VALUES (v_order_id, v_product_id, ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));
  END LOOP;

  COMMIT;
END;


--  DDL for Package MANAGEMENT
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE PACKAGE "SYSTEM"."MANAGEMENT" AS
    -- 函数：计算订单总金额
    FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER;

    -- 存储过程：创建新订单
    PROCEDURE create_new_order(
        p_order_id IN NUMBER,
        p_customer_name IN VARCHAR2,
        p_order_date IN DATE
    );

    -- 存储过程：添加产品
    PROCEDURE add_product(
        p_product_id IN NUMBER,
        p_product_name IN VARCHAR2,
        p_price IN NUMBER
    );
END management;

--  DDL 程序包实体
--------------------------------------------------------
CREATE OR REPLACE
PACKAGE BODY MANAGEMENT AS

  FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER  IS
        v_total_amount NUMBER;
  BEGIN
    -- TODO: FUNCTION MANAGEMENT.calculate_order_total所需的实施
     SELECT SUM(unit_price * quantity)
        INTO v_total_amount
        FROM order_details
        WHERE order_id = p_order_id;

        RETURN NVL(v_total_amount, 0);
  END calculate_order_total;

  PROCEDURE create_new_order(
        p_order_id IN NUMBER,
        p_customer_name IN VARCHAR2,
        p_order_date IN DATE
    ) AS
  BEGIN
    -- TODO: PROCEDURE MANAGEMENT.create_new_order所需的实施
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
        VALUES (p_order_id, p_customer_name, p_order_date, 0);
  END create_new_order;

  PROCEDURE add_product(
        p_product_id IN NUMBER,
        p_product_name IN VARCHAR2,
        p_price IN NUMBER
    ) AS
  BEGIN
    -- TODO: PROCEDURE MANAGEMENT.add_product所需的实施
   INSERT INTO products (product_id, product_name, price)
   
           VALUES (p_product_id, p_product_name, p_price);
  END add_product;

END MANAGEMENT;


--  DDL 测试计算商品总金额
--------------------------------------------------------
DECLARE
  v_order_total NUMBER;
BEGIN
  v_order_total := MANAGEMENT.calculate_order_total(p_order_id => 1);
  DBMS_OUTPUT.PUT_LINE('订单总金额：' || v_order_total);
END;


