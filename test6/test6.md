学号：202010414320  姓名：魏其  班级：3班

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|


### 1.创建表空间
- wq1
```sql
Create Tablespace wq1
datafile
'/home/oracle/app/oracle/oradata/orcl/wq1_1.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/wq1_2.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```
- wq2
```sql
Create Tablespace wq2
datafile
'/home/oracle/app/oracle/oradata/orcl/wq2_1.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/wq2_2.dbf'
  SIZE 400M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```
![](../test6/pict1.png)

### 2.创建表
- 商品表
```sql
CREATE TABLE products (
  product_id NUMBER(10) PRIMARY KEY,
  product_name VARCHAR2(100) NOT NULL,
  category VARCHAR2(50),
  price NUMBER(10, 2) NOT NULL,
  stock_quantity NUMBER(10) NOT NULL
);
```
- 顾客表
```sql
CREATE TABLE customers (
  customer_id NUMBER(10) PRIMARY KEY,
  customer_name VARCHAR2(50) NOT NULL,
  email VARCHAR2(100) UNIQUE NOT NULL,
  phone_number VARCHAR2(15),
  address VARCHAR2(255)
);
```

- 订单表
```sql
CREATE TABLE orders (
  order_id NUMBER(10) PRIMARY KEY,
  customer_id NUMBER(10) NOT NULL,
  order_date DATE DEFAULT SYSDATE NOT NULL,
  total_amount NUMBER(10, 2) NOT NULL,
  FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);
```
- 订单明细表
```sql
CREATE TABLE order_details (
  order_detail_id NUMBER(10) PRIMARY KEY,
  order_id NUMBER(10) NOT NULL,
  product_id NUMBER(10) NOT NULL,
  quantity NUMBER(10) NOT NULL,
  unit_price NUMBER(10, 2) NOT NULL,
  FOREIGN KEY (order_id) REFERENCES orders(order_id),
  FOREIGN KEY (product_id) REFERENCES products(product_id)
);
```
![](../test6/pict2.png)

### 插入数据
- 商品表
```sql
BEGIN
  FOR i IN 1..40000 LOOP
    INSERT INTO products (product_id, product_name, price, stock_quantity)
    VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;
```
- 顾客表
```sql
BEGIN
  FOR i IN 1..40000 LOOP
    INSERT INTO customers (customer_id, customer_name, email, address)
    VALUES (i, 'Customer ' || i,  i || 'qq.com', 'Address ' || i);
  END LOOP;
  COMMIT;
END;

```

- 订单表
```sql
BEGIN
  FOR i IN 1..80000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 40000), 0),  TO_DATE('2000-01-01', 'YYYY-MM-DD') +
  DBMS_RANDOM.VALUE(0, (SYSDATE - TO_DATE('2000-01-01', 'YYYY-MM-DD'))), ROUND(DBMS_RANDOM.VALUE(10, 1000), 0));
  END LOOP;
  COMMIT;
END;

```

- 订单详情表
```sql
DECLARE
  v_order_id NUMBER;
  v_product_id NUMBER;
  v_count NUMBER; -- 添加此行声明v_count变量
BEGIN
  FOR i IN 1..40000 LOOP
    LOOP
      v_order_id := ROUND(DBMS_RANDOM.VALUE(1, 40000), 0);
      v_product_id := ROUND(DBMS_RANDOM.VALUE(1, 40000), 0);

      -- 检查组合是否已存在
      SELECT COUNT(*)
      INTO   v_count
      FROM   order_details
      WHERE  order_id = v_order_id
      AND    product_id = v_product_id;

      -- 如果组合不存在，则插入数据并跳出循环
      IF v_count = 0 THEN
        EXIT;
      END IF;
    END LOOP;

    INSERT INTO order_details (order_id, product_id, quantity, unit_price)
    VALUES (v_order_id, v_product_id, ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));
  END LOOP;

  COMMIT;
END;
```
![](../test6/pict4.png)
![](../test6/pict5.png)

### 设计权限及用户分配方案
```sql
 创建管理员用户
CREATE USER admin IDENTIFIED BY 111;
GRANT CONNECT, RESOURCE, DBA，CREATE SESSION TO admin;

 创建普通用户
CREATE USER wq IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE TO wq;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO wq;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO wq;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO wq;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO wq;
```
![](../test6/pict3.png)

### 程序包
```sql
CREATE OR REPLACE PACKAGE order_management AS
    -- 函数：计算订单总金额
    FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER;

    -- 存储过程：创建新订单
    PROCEDURE create_new_order(
        p_order_id IN NUMBER,
        p_customer_name IN VARCHAR2,
        p_order_date IN DATE
    );

    -- 存储过程：添加产品
    PROCEDURE add_product(
        p_product_id IN NUMBER,
        p_product_name IN VARCHAR2,
        p_price IN NUMBER
    );
END order_management;

# 程序包实体
CREATE OR REPLACE
PACKAGE BODY MANAGEMENT AS

  FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER  IS
        v_total_amount NUMBER;
  BEGIN
    -- TODO: FUNCTION MANAGEMENT.calculate_order_total所需的实施
     SELECT SUM(unit_price * quantity)
        INTO v_total_amount
        FROM order_details
        WHERE order_id = p_order_id;

        RETURN NVL(v_total_amount, 0);
  END calculate_order_total;

  PROCEDURE create_new_order(
        p_order_id IN NUMBER,
        p_customer_name IN VARCHAR2,
        p_order_date IN DATE
    ) AS
  BEGIN
    -- TODO: PROCEDURE MANAGEMENT.create_new_order所需的实施
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
        VALUES (p_order_id, p_customer_name, p_order_date, 0);
  END create_new_order;

  PROCEDURE add_product(
        p_product_id IN NUMBER,
        p_product_name IN VARCHAR2,
        p_price IN NUMBER
    ) AS
  BEGIN
    -- TODO: PROCEDURE MANAGEMENT.add_product所需的实施
   INSERT INTO products (product_id, product_name, price)
   
           VALUES (p_product_id, p_product_name, p_price);
  END add_product;

END MANAGEMENT;
```
![](../test6/pict6.png)

### 测试计算选择商品金额
```sql
DECLARE
  v_order_total NUMBER;
BEGIN
  v_order_total := MANAGEMENT.calculate_order_total(p_order_id => 1);
  DBMS_OUTPUT.PUT_LINE('订单总金额：' || v_order_total);
END;
```
![](../test6/pict7.png)
### 测试添加商品
```sql
DECLARE
  v_product_id NUMBER := 80001;
  v_product_name VARCHAR2(50) := 'productA';
  v_price NUMBER := 88.88;
BEGIN
  MANAGEMENT.add_product(v_product_id, v_product_name, v_price);
  DBMS_OUTPUT.PUT_LINE('已添加新产品：' || v_product_name);
END;
```
![](../test6/pict8.png)
## 备份方案
1. 完全备份：每周一次，备份整个数据库，备份文件存储到wq1表空间中的备份分区。
2. 增量备份：每天晚上备份前一天的增量数据，备份文件存储到wq2表空间中的备份分区。
3. 自动备份：通过Oracle自带的RMAN命令，定期执行备份操作，保证备份数据的完整性。
4. 定时清理：每周自动定时清理过期备份数据，保证数据库备份数据的存储空间不会无限扩大。
5. 数据库存储：将备份数据存储到独立的备份服务器或网络存储设备上，以防止数据损失和灾害恢复。
6. 备份验证：定期恢复备份数据到测试环境，验证备份数据是否能够正常恢复，并修复可能出现的问题
7. 增加冷备份：定期（例如每月一次）将备份文件拷贝到磁盘或其他外部媒介上，并存放在安全的地方，以备份数据在网络存储设备失效等情况下，仍能保持完整性。
8. 增加灾备方案：在备份服务器或其他网络存储设备上，设置灾备服务器，确保在灾难性事件发生时，能够快速恢复数据。同时，也要定期测试灾备方案是否可行，确保在关键时刻能够顺利恢复数据。
9. 增加备份监控：使用Oracle Enterprise Manager等监控软件，对备份方案进行监控和管理。及时发现备份故障和问题，并采取相应的措施，保证备份方案的稳定性和可靠性。

## 总结
此次基于Oracle数据库的商品销售系统我创建了四个表：商品表 (products)，顾客表 (customers)，订单表 (orders)，订单明细表 (order_details)。这些表定义了数据库模式中不同实体之间的关系，通过主键和外键约束确保数据的完整性和一致性。插入了大量数据到这些表中，以模拟实际的数据。商品表和顾客表各插入了40,000条数据，订单表插入了80,000条数据，订单明细表根据已存在的订单和产品组合插入了40,000条数据。设计了权限及用户分配方案。创建了一个管理员用户 (admin)，并授予了CONNECT、RESOURCE、DBA和CREATE SESSION权限。另外创建了一个普通用户 (wq)，并授予了CONNECT和RESOURCE权限，以及对商品表、顾客表、订单表和订单明细表的SELECT、INSERT、UPDATE和DELETE权限。创建了一个名为 order_management 的程序包，其中包含了三个子程序：calculate_order_total 函数、create_new_order 存储过程和 add_product 存储过程。这些子程序提供了一些特定的业务逻辑和数据操作功能，如计算订单总金额、创建新订单和添加产品。通过以上操作，我们完成了一个简单的数据库模型的创建、数据插入和权限分配，并设计了一些程序包来支持一些特定的业务逻辑。


